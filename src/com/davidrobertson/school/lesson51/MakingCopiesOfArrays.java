package com.davidrobertson.school.lesson51;


import java.util.Random;
import java.util.Scanner;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author David 
 */
public class MakingCopiesOfArrays {
    public static void main(String[] args) 
    {
        // /*
        //in the coding with arrays exercise, we learned about the fact 
        //that arrays are referenced in memory, and therefore if you try 
        //to copy one, your operations on one will also affect the other.
        //let's review:
        System.out.println("******* SECTION ONE**************");
        System.out.println("Populating numbers:");
        int[] numbers = new int[10]; //has a length of 10, indexes 0-9
        Random r = new Random();
        for (int i = 0; i < numbers.length; i++)
        {
            numbers[i] = r.nextInt(100000);
        }
        
        //"copy" the array directly.
        System.out.println("Ctrl+c, Ctrl+v-ing...");
        int[] numbers2 = numbers;
        
        //uh-oh, we have a referencing issue:
        numbers[3] = 999;
        numbers2[7] = 1000;
        
        System.out.println("Printing numbers:");
        for (int n : numbers)
        {
            System.out.printf("The next number is %d\n", n);
        }
        
        System.out.println(Stars(30));
        System.out.println("Printing numbers 2:");
        for (int n : numbers2)
        {
            System.out.printf("The next number is %d\n", n);
        }
        
        System.out.println(Stars(50));
        
        
       
    }
    
    /**
     * Print out a string of stars based on passed in length
     * @param num
     * @return
     */
    public static String Stars(int num)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < num; i++)
        {
            sb.append("*");
        }
        return sb.toString();
    }
}
