package com.davidrobertson.school.lesson52;


import java.util.*;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author David 
 */
public class SortingArrays 
{
    public static void main(String[] args)
    {
        
        Random r = new Random();
        int[] numbers = new int[15];
        for (int i = 0; i < numbers.length; i++)
        {
            numbers[i] = r.nextInt(10000);
        }
        
        System.out.println("Unsorted..");
        for (int i : numbers)
        {
            System.out.printf("Next Number: %d\n", i);
        }
        
        SortArrayUsingInsertionSort(numbers); //remember, this is by reference!
        System.out.println("Sorted..");
        for (int i : numbers)
        {
            System.out.printf("Next Number: %d\n", i);
        }
        System.out.println(Stars(30));
        
        //of course, we don't have to re-invent the wheel:
        String[] lastNames = new String[10];
        lastNames[0] = "Aiden";
        lastNames[1] = "Brett";
        lastNames[2] = "Hunter";
        lastNames[3] = "Josh";
        lastNames[4] = "Caleb";
        lastNames[5] = "Simian";
        lastNames[6] = "Andrew";
        lastNames[7] = "Silas";
        lastNames[8] = "Landon";
        lastNames[9] = "Harrison";
        
        Arrays.sort(lastNames);
        for (String s : lastNames)
        {
            System.out.printf("Next Name: %s\n", s);
        }
        System.out.println(Stars(30));
        
        //and of course if you wanted to sort just a few, you could
        numbers = new int[10];
        for (int i = 0; i < numbers.length; i++)
        {
            numbers[i] = r.nextInt(10000);
        }
        
        Arrays.sort(numbers, 3, 7);
        for (int i = 0; i < numbers.length; i++)
        {
            if (i > 1 && i < 9)
            {
                System.out.printf("Next Number: %d\n", numbers[i]);
            }
        }
    }
    
    
    private static void SortArrayUsingInsertionSort(int[] data)
    {
        //there is nothing to the left of [0] so we can't sort it
        //therefore, start at 1:
        for (int i = 1; i < data.length; i++)
        {
            //work in reverse from current position, sliing left
            //if the number to the right is larger
            for (int j = i; j > 0 && data[j] < data[j-1]; j--)
            {
                /*
                System.out.printf("%d < %d, sliding left...\n"
                                    , data[j], data[j-1]);
                */
                int temp = data[j-1];
                data[j-1] = data[j];
                data[j] = temp;
            }
        }
    }
    
    /**
     * Print out a string of stars based on passed in length
     * @param num
     * @return
     */
    public static String Stars(int num)
    {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < num; i++)
        {
            sb.append("*");
        }
        return sb.toString();
    }
}
