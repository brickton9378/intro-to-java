/**
 * Storing Types in Variables Part 1.
 * @author Brian
 */
package com.davidrobertson.school.lesson16;

public class StoringTypesInVariablesPart1 
{
    public static void main(String[] args)
    {
        System.out.println("Types in Variables:");
        System.out.println("****************************");
        System.out.println("Integers");
        
        int index = 32;
        System.out.printf("Index = %d\n", index);
        index = 10;
        System.out.printf("Index = %d\n", index);
        
        index = index + 1;
        System.out.printf("Index = %d\n", index);
        index++;
        System.out.printf("Index = %d\n", index);
        index+=1;
        System.out.printf("Index = %d\n", index);
        index+=30;
        System.out.printf("Index = %d\n", index);
        
        index = index - 1;
        System.out.printf("Index = %d\n", index);
        index--;
        System.out.printf("Index = %d\n", index);
        index-=1;
        System.out.printf("Index = %d\n", index);
        index-=30;
        System.out.printf("Index = %d\n", index);
        
        index*=3;
        System.out.printf("Index = %d\n", index);
        index/=3;
        System.out.printf("Index = %d\n", index);
        
        //Integer
        Integer x = 32;
        System.out.printf("x = %d\n", x);
        x+=25;
        System.out.printf("x = %d\n", x);
        x+=x;
        
        //remember, max value of int is 2,147,483,647 - so this won't work!
        //int bigNumber = 999999999999;
        System.out.println("****************************");
        
        
        
    }
}
