/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.davidrobertson.school.lesson12;
/**
 *
 * @author David
 */
public class SudokuGrid2 {
    public static void main(String[] args)
    {
      
        
        String horizontalLine =  "XXXXXXXXXXXXXXXXXXXXX"; //
        String one = "1";
        String two = "2";
        String three = "3";
        String four = "4";
        String five = "5";
        String six = "6";
        String seven = "7";
        String eight = "8";
        String nine = "9";
        //we can use String.format to make a replacement pattern (just like
        // if the pattern was in our printf statement):
        String gridData = "%s:%s:%s";
        
        //String part1 = String.format(gridData, one, two, three);
        
        System.out.printf("%s | %s | %s\n"
                            , String.format(gridData, three, four, five)
                            , String.format(gridData, six, seven, eight)
                            , String.format(gridData, nine, one, two));
        System.out.printf("%s | %s | %s\n"
                            , String.format(gridData, six, seven, eight)
                            , String.format(gridData, nine, one, two)
                            , String.format(gridData, three, four, five));
        System.out.printf("%s | %s | %s\n"
                            , String.format(gridData, nine, one, two)
                            , String.format(gridData, three, four, five)
                            , String.format(gridData, six, seven, eight));
        System.out.println(horizontalLine);
        System.out.printf("%s | %s | %s\n"
                            , String.format(gridData, two, three, four)
                            , String.format(gridData, five, six, seven)
                            , String.format(gridData, eight, nine, one));
        System.out.printf("%s | %s | %s\n"
                            , String.format(gridData, five, six, seven)
                            , String.format(gridData, eight, nine, one)
                            , String.format(gridData, two, three, four));
        System.out.printf("%s | %s | %s\n"
                            , String.format(gridData, eight, nine, one)
                            , String.format(gridData, two, three, four)
                            , String.format(gridData, five, six, seven));
        System.out.println(horizontalLine);
        System.out.printf("%s | %s | %s\n"
                , String.format(gridData, one, two, three)
                , String.format(gridData, four, five, six)
                , String.format(gridData, seven, eight, nine));
        System.out.printf("%s | %s | %s\n"
                , String.format(gridData, four, five, six)
                , String.format(gridData, seven, eight, nine)
                , String.format(gridData, one, two, three));
        System.out.printf("%s | %s | %s\n"
                , String.format(gridData, seven, eight, nine)
                , String.format(gridData, one, two, three)
                , String.format(gridData, four, five, six));
    }
}
