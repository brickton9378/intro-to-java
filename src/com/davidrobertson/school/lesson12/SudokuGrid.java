
package com.davidrobertson.school.lesson12;
/**
 *
 * @author David
 */
public class SudokuGrid 
{
    public static void main(String[] args)
    {
        System.out.println("3:4:5 | 6:7:8 | 9:1:2");
        System.out.println("6:7:8 | 9:1:2 | 3:4:5");
        System.out.println("9:1:2 | 3:4:5 | 6:7:8");
        System.out.println("---------------------");
        System.out.println("2:3:4 | 5:6:7 | 8:9:1");
        System.out.println("5:6:7 | 8:9:1 | 2:3:4");
        System.out.println("8:9:1 | 2:3:4 | 5:6:7");
        System.out.println("---------------------");
        System.out.println("1:2:3 | 4:5:6 | 7:8:9");
        System.out.println("4:5:6 | 7:8:9 | 1:2:3");
        System.out.println("7:8:9 | 1:2:3 | 4:5:6");
        
    }
}