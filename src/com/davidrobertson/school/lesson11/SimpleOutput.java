package com.davidrobertson.school.lesson11;
/**
 *
 * @author David
 */
public class SimpleOutput 
{
    public static void main(String[] args)
    {
        //println
        /*output on a single line using System.out.println("...") 
            -- note that we are printing out a String literal
            -- the output must be contained in ""'s or be a String type
         */
        System.out.println("------------  println     -----------");
        System.out.println("We're really moving "
                            + "fast now! ALMOST LIGHT SPEEEEEEEEEEEEEEEEEED!!!!");
        
        /*
         * Formatted output:
         *  %s ==> String
         *  \t ==> tab space in output
         *  \n ==> newline in output
         */
        
        System.out.println("Welcome to Sudoku sir/madam\nPlease Enter your name:\tDavid\n");
        
        //printf
        System.out.println("------------  printf  -----------");
        System.out.printf("Hello %s\n", "David");
        System.out.println("-------------------------------------");
        System.out.printf("%s\n%s\t%s\n"
        					, "Welcome to the Sudoku program, sir/madam.\n"
        					, "Please enter your name:\n"
        					, "David");
        System.out.println("-------------------------------------");
        
        
        //if you want multiple lines, don't forget the /n!
        System.out.printf("%s", "Welcome to the Sudoku program, sir/madam.");
        System.out.printf("%s", "Please enter your name:\n\n\n");
        
       
        System.out.println("-------------------------------------");
        System.out.print("Hello\n");
        System.out.println("-------------------------------------");
    }
}
