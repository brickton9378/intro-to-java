/**
 * Storing Types in Variables Part 2.
 * @author Brian
 */
package com.davidrobertson.school.lesson18;


public class StoringTypesInVariablesPart2 {

    public static void main(String[] args)
    {
        System.out.println("Types in Variables 2:");
        System.out.println("****************************");
        System.out.println("Characters");
        //char 
        char one = 'b';
        char two = '3';
        char three = 'B';
        char four = '#';
        System.out.println(one);
        System.out.println(two);
        System.out.println(three);
        System.out.println(four);
        
        //String s2 = one + two + three + four;
        
        //String s2 = one.toString() + two.toString() 
        //            + three.toString() + four.toString();
        //System.out.println(s2);
        
        String finalString = String.format("%c%c%c%c", one, two, three, four);
        System.out.println(finalString);
        
        //full Character de-referencing
        Character oneFull = 'b';
        Character twoFull = '3';
        Character threeFull = 'B';
        Character fourFull = '#';
        String s4 = oneFull.toString() + twoFull.toString()
                        + threeFull.toString() + fourFull.toString();
        System.out.println(s4);
        
        //String s5 = one.toString() + two.toString()
        //			+ three.toString() + four.toString();
         
        String s5 = ((Character)one).toString() + ((Character)two).toString() +
                    ((Character)three).toString() + ((Character)four).toString();
 
        System.out.println(s5);  
       
        
    }
}
