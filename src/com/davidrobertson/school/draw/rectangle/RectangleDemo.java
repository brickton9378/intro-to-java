package com.davidrobertson.school.draw.rectangle;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import static javafx.geometry.HPos.RIGHT;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;

public class RectangleDemo extends Application {

	/**
	 * The parent class "Application" has a "start" method and here we override it.
	 */
	@Override
	public void start(Stage primaryStage) {

		/**
		 * For this Window, we chose the Grid layout, where components are added in columns and rows
		 */
		GridPane grid = drawCoordinatesGrid();

		/**
		 * We create a "Group" to group all elements in one place
		 * Notice we add the "Grid" to this Group
		 */
		Group root = new Group();
		root.getChildren().add(grid);

		/**
		 * The Application parent class passes in to us a Stage, where we can add a Scene.
		 * First we create the Scene, adding the "root" Group we created above (we're tying everything together).
		 */
		Scene scene = new Scene(root, 600, 450, Color.WHITE);
		primaryStage.setTitle("The Rectangle");
		primaryStage.setScene(scene);
		primaryStage.show();
	}

	/**
	 * Main Class
	 */
	public static void main(String[] args) {
		Application.launch(args); //we call the parent class (Application) to do the heavy lifting for us
	}


	/**
	 * We create a separate method to draw a rectangle. Notice the X and Y (top and left position) are set to 0.
	 * Rectangle is a class of JavaFX (javafx.scene.shape.Rectangle, see import statement above)
	 * @param width
	 * @param height
	 * @return
	 */
	private Rectangle drawRectangle(double width, double height) {

		Rectangle rectangle = new Rectangle();
		rectangle.setX(0);
		rectangle.setY(0);
		rectangle.setWidth(width);
		rectangle.setHeight(height);
		rectangle.setFill(Color.BLACK);

		return rectangle;	

	}

	/**
	 * Creating the grid is more involving, as we add individual elements (components) to columns and rows.
	 * @return
	 */
	private GridPane drawCoordinatesGrid() {
		
		/**
		 * Setup the grid
		 */
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.setPadding(new Insets(25, 25, 25, 25));

		/**
		 * Create the first element, a label
		 */
		Text scenetitle = new Text("Hello there!");
		scenetitle.setFont(Font.font("Ronald", FontWeight.NORMAL, 20));
		/**
		 * We add the title to the first row and column (0,0) the 2 spans 2 columns (merges them) and 1 spans 1 row.
		 */
		grid.add(scenetitle, 0, 0, 2, 1); //add(Node child, int columnIndex, int rowIndex, int colspan, int rowspan)

		/**
		 * Create another label, "Width"
		 */
		Label rectWidthLabel = new Label("Width:");
		grid.add(rectWidthLabel, 0, 1); //column 0, row 1

		/**
		 * Here we create a TextField, a field where user can enter (type-in) a value. Notice we're not validating anything.
		 */
		TextField rectWidthField = new TextField();
		grid.add(rectWidthField, 1, 1); //column 1, row 1

		/**
		 * Create another label, "Height"
		 */

		Label rectHeightLabel = new Label("Height:");
		grid.add(rectHeightLabel, 0, 2); //column 0, row 2

		TextField rectHeightField = new TextField();
		grid.add(rectHeightField, 1, 2); //column 1, row 2

		/**
		 * Here we add a button
		 * Notice the button is created inside an HBox. This is like Russian Dolls...
		 * Then we add the HBox (with the button!) to the grid, column 1, row 4
		 */
		Button btn = new Button("Create");
		HBox hbBtn = new HBox(10);
		hbBtn.setAlignment(Pos.BOTTOM_RIGHT);
		hbBtn.getChildren().add(btn);
		grid.add(hbBtn, 1, 4);   //column 1, row 4

		/**
		 * Create another label to display the rectangle area
		 */
		final Text rectAreaLabel = new Text();
		grid.add(rectAreaLabel, 0, 6);
		grid.setColumnSpan(rectAreaLabel, 2);
		grid.setHalignment(rectAreaLabel, RIGHT);
		rectAreaLabel.setId("rectAreaLabel");

		/**
		 * Here we finally draw the rectangle to column 0, row 7. Notice we span 2 columns with setter injection
		 */
		final Rectangle rectangleShape = drawRectangle(1,1);
		grid.add(rectangleShape, 0, 7);
		grid.setColumnSpan(rectangleShape, 2);
		grid.setHalignment(rectangleShape, RIGHT);
		rectangleShape.setId("rectangleShape");

		/**
		 * Back to the button. Each button has a "setOnAction" method, meaning when it's clicked. Whenever the button is clicked, 
		 * the JavaFX will will run the code we set in there. Notice the setter value is an entire "anonymous class" where you declare and
		 * instantiate a class at the same time. This is a separate topic to be learnt, but anonymous classes goal is to make the code more concise.
		 */
		btn.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				//actiontarget.setFill(Color.FIREBRICK);
				//System.out.printf("user name: %s - Password: %s", rectWidthField.getText(), rectHeightField.getText());
				double width = Double.parseDouble(rectWidthField.getText());
				double height = Double.parseDouble(rectHeightField.getText());
				rectangleShape.setWidth(width);
				rectangleShape.setHeight(height);
				rectAreaLabel.setText("Your rectangle's area is: " + (width * height));

			}
		});

		/**
		 * When we're done, we just return the entire grid
		 */
		return grid;

	}

}
