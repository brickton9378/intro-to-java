package com.davidrobertson.school.lesson22;

import java.util.*;

/**
 * Calculate Momentum
 * @author David
 */
public class MomentumCalculator {
    //momentum = mass * velocity
    public static void main(String[] args){
        Scanner input = new Scanner(System.in);
        double mass;
        double velocity;
        double momentum;
        System.out.println("Welcome to the momentum calculator sir/madam!");
        System.out.println("Please enter the mass (in kg):");
        mass = input.nextDouble();
        System.out.println("Please enter the velocity (in meters per second):");
        velocity = input.nextDouble();
        momentum = mass * velocity;
        System.out.printf("The momentum is %.5f kg*m/s!\n", momentum);
        input.close();
    }
}
