package com.davidrobertson.school.lesson19;

//Here we import the java.util package!
//http://docs.oracle.com/javase/8/docs/api/java/util/Scanner.html

import java.util.Scanner;
//import java.util.*;



/**
 * Create a Scanner to get user input.
 * @author David
 */
public class SimpleInput {

    public static void main(String[] args)
    {
        String name = null;
        
        Scanner input = new Scanner(System.in);
        System.out.println("Please enter your name sir/madam:");
        name = input.nextLine();
        
        System.out.printf("Hello, Your name is: %s\n", name);
        input.close();
    }
}
